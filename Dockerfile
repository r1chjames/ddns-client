FROM golang:1.17-alpine

LABEL maintainer="Richard James<richjames11@gmail.com>"

RUN apk add make git

COPY . .
RUN make build

# Run the executable
CMD ["/go/bin/ddns-client"]
