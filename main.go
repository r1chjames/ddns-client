package main

import (
	"fmt"
	"gitlab.com/r1chjames/ddns-client/rest"
	"os"
	"time"
)

func main() {
	updateUrl := getEnv("UPDATE_URL", "")
	ipFetchUrl := getEnv("IP_FETCH_URL", "https://ifconfig.co")
	runMode := getEnv("RUN_MODE", "")
	recordSetName := getEnv("RECORD_SET_NAME", "")
	apiKey := getEnv("API_KEY", "")
	fmt.Println("Update URL: ", updateUrl)

	switch runMode {
	case "SCHEDULED":
		duration := getEnv("INTERVAL", "")
		executeScheduledJob(updateUrl, ipFetchUrl, duration, recordSetName, apiKey)
	case "SINGLE":
		execute(updateUrl, ipFetchUrl, recordSetName, apiKey)
	default:
		fmt.Println("Must specify a RUN_MODE")
	}
}

func execute(updateUrl string, ipFetchUrl string, recordSetName string, apiKey string) {
	currentIp := rest.GetIpAddress(ipFetchUrl)
	fmt.Println("Current IP:", currentIp)
	rest.SendPostMessage(updateUrl, apiKey, rest.UpdateMessage{RecordSetIp: currentIp, RecordSetName: recordSetName})
}

func executeScheduledJob(updateUrl string, ipFetchUrl string, interval string, recordSetName string, apiKey string) {
	duration, _ := time.ParseDuration(interval)
	doEvery(duration, func() {
		execute(updateUrl, ipFetchUrl, recordSetName, apiKey)
	})
}

func doEvery(d time.Duration, f func()) {
	for range time.Tick(d) {
		f()
	}
}

func getEnv(key, fallback string) string {
	value := os.Getenv(key)
	if len(value) == 0 {
		return fallback
	}
	return value
}
