package rest

type UpdateMessage struct {
	RecordSetIp   string `json:"recordSetIp"`
	RecordSetName string `json:"recordSetName"`
}
